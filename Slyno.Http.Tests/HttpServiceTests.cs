﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Slyno.Loggers;

namespace Slyno.Http.Tests
{
    [TestClass]
    public class HttpServiceTests
    {
        private readonly StringBuilderLogger _logger = new StringBuilderLogger
        {
            Verbosity = LogType.Debug
        };

        [TestMethod]
        public async Task GetGoogle_ShouldBeOk()
        {
            var http = new HttpService
            {
                BaseUri = new Uri("https://google.com"),
                Logger = _logger
            };

            var response = await http.SendAsync(new HttpMethod(Method.Get), "", CancellationToken.None);

            await http.ValidateResponseAsync(response);

            Debug.WriteLine(_logger.GetLog());

            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);
        }

        [TestMethod]
        public async Task GetGoogle_GetWithBearerToken_ShouldSetAuthHeader()
        {
            var http = new HttpService
            {
                BaseUri = new Uri("https://google.com"),
                Logger = _logger
            };

            var headers = new Dictionary<string, string>
            {
                {"Authorization", "Bearer test"}
            };

            await http.SendAsync(new HttpMethod(Method.Get), "", CancellationToken.None, null, headers);

            var log = _logger.GetLog();

            Debug.WriteLine(log);

            Assert.IsTrue(log.Contains("Authorization: Bearer test"));
        }


        [TestMethod]
        public async Task PostWithData_ShouldSetContent()
        {
            var http = new HttpService
            {
                BaseUri = new Uri("https://google.com"),
                Logger = _logger
            };

            var json = new { Data = "data" };

            await http.SendAsync(new HttpMethod(Method.Post), "", CancellationToken.None, new JsonContent(json));

            var log = _logger.GetLog();

            Debug.WriteLine(log);

            Assert.IsTrue(log.Contains("{\"Data\":\"data\"}"));
        }

        [TestMethod]
        public async Task ChangeVerbosity_ShouldHideAuthHeader()
        {
            var http = new HttpService
            {
                BaseUri = new Uri("https://google.com"),
                Logger = _logger
            };

            var headers = new Dictionary<string, string>
            {
                {"Authorization", "Bearer test"}
            };

            await http.SendAsync(new HttpMethod(Method.Get), "", CancellationToken.None, null, headers);

            var log = _logger.GetLog();

            Debug.WriteLine(log);

            Assert.IsTrue(log.Contains("Authorization: Bearer test"));

            var temp = _logger.Verbosity;
            _logger.Verbosity = LogType.Information;

            await http.SendAsync(new HttpMethod(Method.Get), "", CancellationToken.None, null, headers);

            _logger.Verbosity = temp;

            log = _logger.GetLog();

            Debug.WriteLine(log);

            Assert.IsTrue(!log.Contains("Authorization: Bearer test"));
        }
    }
}
