﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Slyno.Loggers;

namespace Slyno.Http
{
    public interface IHttpService
    {
        ILogger Logger { get; set; }

        IDictionary<string, string> DefaultRequestHeaders { get; }

        Uri BaseUri { get; set; }

        TimeSpan Timeout { get; set; }

        void CancelPendingRequests();

        Task<T> GetAsync<T>(string relativeUri, IDictionary<string, string> headers = null);

        Task<T> PatchAsync<T>(string relativeUri, HttpContent content, IDictionary<string, string> headers = null);

        Task<T> PostAsync<T>(string relativeUri, HttpContent content = null, IDictionary<string, string> headers = null);

        Task<T> PutAsync<T>(string relativeUri, HttpContent content, IDictionary<string, string> headers = null);

        Task DeleteAsync(string relativeUri, IDictionary<string, string> headers = null);

        Task<HttpResponseMessage> SendAsync(HttpMethod method, string relativeUri, CancellationToken cancellationToken, HttpContent content = null, IDictionary<string, string> headers = null);

        Task ValidateResponseAsync(HttpResponseMessage response);
    }
}
