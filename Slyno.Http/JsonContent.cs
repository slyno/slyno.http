﻿using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace Slyno.Http
{
    public class JsonContent : StringContent
    {
        /// <summary>
        /// Creates HttpContent that will have the content type of application/json.
        /// </summary>
        /// <param name="entity">Typically pass a dynamic or simple object here. This will get converted to a JSON string. Any DateTimes will be UTC.</param>
        public JsonContent(object entity)
            : this(entity, new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Utc, ReferenceLoopHandling = ReferenceLoopHandling.Ignore })
        {
        }

        /// <summary>
        /// Creates HttpContent that will have the content type of application/json.
        /// </summary>
        /// <param name="entity">Typically pass a dynamic or simple object here. This will get converted to a JSON string using the given serializer settings.</param>
        /// <param name="settings">Serializer settings to use.</param>
        public JsonContent(object entity, JsonSerializerSettings settings) :
            base(JsonConvert.SerializeObject(entity, settings))
        {
            this.Headers.ContentType = new MediaTypeHeaderValue(MediaType.ApplicationJson);
        }
    }
}
