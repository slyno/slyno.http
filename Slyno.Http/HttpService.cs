﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Slyno.Loggers;

namespace Slyno.Http
{
    public class HttpService : IHttpService
    {
        // Research shows that keeping a single HttpClient has much better pefermance than wrapping a new one in a using statement.
        private readonly HttpClient _httpClient = new HttpClient
        {
            Timeout = TimeSpan.FromMinutes(5)
        };

        /// <summary>
        /// Gets or sets the logger. Default is a NullLogger with Verbosity set to Error.
        /// </summary>
        public ILogger Logger { get; set; } = new NullLogger
        {
            Verbosity = LogType.Error
        };

        /// <summary>
        /// Get or set headers that will be sent with every request. Accept applicaiton/json is here by default.
        /// </summary>
        public IDictionary<string, string> DefaultRequestHeaders { get; } = new Dictionary<string, string>
        {
            {"Accept", MediaType.ApplicationJson}
        };

        /// <summary>
        /// Gets or sets the base Uri used for every request.
        /// </summary>
        public Uri BaseUri
        {
            get { return _httpClient.BaseAddress; }
            set { _httpClient.BaseAddress = value; }
        }

        /// <summary>
        /// Gets or sets the http timeout. Deafult is 5 minutes.
        /// </summary>
        public TimeSpan Timeout
        {
            get { return _httpClient.Timeout; }
            set { _httpClient.Timeout = value; }
        }

        /// <summary>
        /// Cancels all pending requests.
        /// </summary>
        public void CancelPendingRequests()
        {
            this.Logger?.Log("HttpService cancelling all pending requests.", LogType.Information);
            _httpClient.CancelPendingRequests();
        }

        /// <summary>
        /// Sends a GET request and returns a DTO of the given type.
        /// </summary>
        /// <typeparam name="T">Type of DTO.</typeparam>
        /// <param name="relativeUri">Relative URI to request.</param>
        /// <param name="headers">Headers to add or replace the default request headers.</param>
        public async Task<T> GetAsync<T>(string relativeUri, IDictionary<string, string> headers = null)
        {
            var response = await this.SendAsync(new HttpMethod(Method.Get), relativeUri, CancellationToken.None, null, headers);

            return await this.ParseResponseAsync<T>(response);
        }

        /// <summary>
        /// Sends a PATCH request and returns a DTO of the given type.
        /// </summary>
        /// <typeparam name="T">Type of DTO.</typeparam>
        /// <param name="relativeUri">Relative URI to request.</param>
        /// <param name="content">The content to send. Typically JsonContent.</param>
        /// <param name="headers">Headers to add or replace the default request headers.</param>
        public async Task<T> PatchAsync<T>(string relativeUri, HttpContent content, IDictionary<string, string> headers = null)
        {
            var response = await this.SendAsync(new HttpMethod(Method.Patch), relativeUri, CancellationToken.None, content, headers);

            return await this.ParseResponseAsync<T>(response);
        }

        /// <summary>
        /// Sends a POST request and returns a DTO of the given type.
        /// </summary>
        /// <typeparam name="T">Type of DTO.</typeparam>
        /// <param name="relativeUri">Relative URI to request.</param>
        /// <param name="content">The content to send. Typically JsonContent.</param>
        /// <param name="headers">Headers to add or replace the default request headers.</param>
        public async Task<T> PostAsync<T>(string relativeUri, HttpContent content = null, IDictionary<string, string> headers = null)
        {
            var response = await this.SendAsync(new HttpMethod(Method.Post), relativeUri, CancellationToken.None, content, headers);

            return await this.ParseResponseAsync<T>(response);
        }

        /// <summary>
        /// Sends a PUT request and returns a DTO of the given type.
        /// </summary>
        /// <typeparam name="T">Type of DTO.</typeparam>
        /// <param name="relativeUri">Relative URI to request.</param>
        /// <param name="content">The content to send. Typically JsonContent.</param>
        /// <param name="headers">Headers to add or replace the default request headers.</param>
        public async Task<T> PutAsync<T>(string relativeUri, HttpContent content, IDictionary<string, string> headers = null)
        {
            var response = await this.SendAsync(new HttpMethod(Method.Put), relativeUri, CancellationToken.None, content, headers);

            return await this.ParseResponseAsync<T>(response);
        }

        /// <summary>
        /// Sends a DELETE request.
        /// </summary>
        /// <param name="relativeUri">Relative URI to request.</param>
        /// <param name="headers">Headers to add or replace the default request headers.</param>
        public async Task DeleteAsync(string relativeUri, IDictionary<string, string> headers = null)
        {
            var response = await this.SendAsync(new HttpMethod(Method.Delete), relativeUri, CancellationToken.None, null, headers);

            await this.ValidateResponseAsync(response);
        }

        /// <summary>
        /// Sends a request and returns the raw response. Callers will need to validate that the response was successful if they care to.
        /// </summary>
        /// <param name="method">The method or verb to use.</param>
        /// <param name="relativeUri">The relative URI to send the request to.</param>
        /// <param name="cancellationToken">Cancellation token so this request can be cancelled if needed.</param>
        /// <param name="content">The content to send.</param>
        /// <param name="headers">Additional headers or override DefaultRequestHeaders.</param>
        public async Task<HttpResponseMessage> SendAsync(HttpMethod method, string relativeUri, CancellationToken cancellationToken, HttpContent content = null, IDictionary<string, string> headers = null)
        {
            var request = this.GetHttpRequestAsync(relativeUri, method, content, headers);

            await this.LogRequestAsync(request);

            var response = await _httpClient.SendAsync(request, cancellationToken);

            await this.LogResponseAsync(response);

            return response;
        }

        /// <summary>
        /// Validates a response. This will throw if the status code is an error.
        /// Useful to call this after any call that returns the raw HttpResponseMessage.
        /// </summary>
        /// <param name="response">The response to validate.</param>
        public async Task ValidateResponseAsync(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
            {
                return;
            }

            var json = await response.Content.ReadAsStringAsync();

            var message = JsonConvert.DeserializeObject<HttpServiceMessage>(json) ??
                                     new HttpServiceMessage { Message = response.ReasonPhrase };

            throw new HttpServiceException(response.StatusCode, message);
        }

        private HttpRequestMessage GetHttpRequestAsync(string relativeUri, HttpMethod method, HttpContent content, IDictionary<string, string> requestHeaders)
        {
            var request = new HttpRequestMessage(method, relativeUri);

            if (content != null)
            {
                request.Content = content;
            }

            var finalHeaders = this.DefaultRequestHeaders.ToDictionary(kvp => kvp.Key, pair => pair.Value);

            if (requestHeaders != null)
            {
                foreach (var header in requestHeaders)
                {
                    if (finalHeaders.ContainsKey(header.Key))
                    {
                        finalHeaders[header.Key] = header.Value;
                    }
                    else
                    {
                        finalHeaders.Add(header.Key, header.Value);
                    }
                }
            }

            foreach (var header in finalHeaders)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            return request;
        }

        private async Task<T> ParseResponseAsync<T>(HttpResponseMessage response)
        {
            await this.ValidateResponseAsync(response);

            var json = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(json);
        }

        private async Task LogRequestAsync(HttpRequestMessage request)
        {
            if (this.Logger == null || this.Logger.Verbosity > LogType.Information)
            {
                return;
            }

            var sb = new StringBuilder();
            sb.AppendLine("HttpService Request");
            sb.AppendLine($"{request.Method} {this.BaseUri}{request.RequestUri}");

            var headers = request.Headers;

            foreach (var header in headers.Where(x => x.Key != "Authorization"))
            {
                sb.AppendLine($"{header.Key}: {string.Join("; ", header.Value)}");
            }

            var content = request.Content;
            var mediaType = content?.Headers.ContentType.MediaType;

            if (content != null)
            {
                sb.AppendLine($"Content-Type: {mediaType}");
                sb.AppendLine($"Content-Length: {content.Headers?.ContentLength}");
            }

            if (this.Logger.Verbosity == LogType.Debug)
            {
                var auth = headers.Authorization;

                if (auth != null)
                {
                    sb.AppendLine($"Authorization: {auth.Scheme} {auth.Parameter}");
                }

                if (content != null && (mediaType.Contains("text") || mediaType.Contains("json")))
                {
                    var json = await content.ReadAsStringAsync();

                    sb.AppendLine();
                    sb.AppendLine(json);
                }
            }

            await this.Logger.LogAsync(sb.ToString(), LogType.Information);
        }

        private async Task LogResponseAsync(HttpResponseMessage response)
        {
            if (this.Logger == null || this.Logger.Verbosity > LogType.Information)
            {
                return;
            }

            var request = response.RequestMessage;

            var sb = new StringBuilder();
            sb.AppendLine("HttpService Response");
            sb.AppendLine($"{request.Method} {request.RequestUri}");

            foreach (var header in response.Headers)
            {
                sb.AppendLine($"{header.Key}: {string.Join("; ", header.Value)}");
            }

            var content = response.Content;
            var mediaType = content?.Headers.ContentType.MediaType;

            if (content != null)
            {
                sb.AppendLine($"Content-Type: {mediaType}");
                sb.AppendLine($"Content-Length: {content.Headers?.ContentLength}");
            }

            if (this.Logger.Verbosity == LogType.Debug && content != null && (mediaType.Contains("text") || mediaType.Contains("json")))
            {
                var json = await content.ReadAsStringAsync();

                sb.AppendLine();
                sb.AppendLine(json);
            }

            await this.Logger.LogAsync(sb.ToString(), LogType.Information);
        }
    }
}
