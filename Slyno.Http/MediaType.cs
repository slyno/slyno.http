﻿namespace Slyno.Http
{
    /// <summary>
    /// Common MediaTypes
    /// </summary>
    public static class MediaType
    {
        public const string Any = "*/*";

        public const string ApplicationAtom = "application/atom+xml";
        public const string ApplicationGzip = "application/gzip";
        public const string ApplicationJavascript = "application/javascript";
        public const string ApplicationJson = "application/json";
        public const string ApplicationOctetStream = "application/octet-stream";
        public const string ApplicationPdf = "application/pdf";
        public const string ApplicationXWwwFormUrlEncoded = "application/x-www-form-urlencoded";
        public const string ApplicationZip = "application/zip";

        public const string AudioMp4 = "audio/mp4";
        public const string AudioMpeg = "audio/mpeg";
        public const string AudioOgg = "audio/ogg";
        public const string AudioWebm = "audio/webm";

        public const string FontEot = "application/vnd.ms-fontobject";
        public const string FontOtf = "application/font-sfnt";
        public const string FontTtf = "application/font-sfnt";
        public const string FontWoff = "applicatoin/font-woff";

        public const string ImageBmp = "image/bmp";
        public const string ImageGif = "image/gif";
        public const string ImageJpeg = "image/jpeg";
        public const string ImagePng = "image/png";
        public const string ImageSvg = "image/svg+xml";

        public const string MultipartFormData = "multipart/form-data";

        public const string TextCss = "text/css";
        public const string TextCsv = "text/csv";
        public const string TextHtml = "text/html";
        public const string TextPlain = "text/plain";
        public const string TextXml = "text/xml";

        public const string VideoAvi = "video/avi";
        public const string VideoMp4 = "video/mp4";
        public const string VideoMpeg = "video/mpeg";
        public const string VideoOgg = "video/ogg";
        public const string VideoWebm = "video/webm";
    }
}
