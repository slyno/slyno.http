﻿using System;
using System.Net;

namespace Slyno.Http
{
    public class HttpServiceException : Exception
    {
        public HttpServiceException(HttpStatusCode statusCode, HttpServiceMessage message)
            : base(message.Message)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCode StatusCode { get; private set; }
    }
}
