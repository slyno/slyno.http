Library includes some base helpers including:

HttpService to help with HttpClient. This includes added Patch support and JsonContent.

[Nuget package available](https://www.nuget.org/packages/Slyno.Http/).
